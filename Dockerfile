FROM scratch
LABEL maintainer="patrickfdomnick@gmail.com"
# COPY --from=build /go/bin/goreleaser /
COPY goreleaser /
EXPOSE 8080/tcp
# ENTRYPOINT ["/goreleaser"]
